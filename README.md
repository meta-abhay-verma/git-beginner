# Git - Beginner

Git is a distributed version control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows. [Wikipedia](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjz_MnK5c_sAhVrxDgGHdrOBmwQmhMwHnoECBUQAg&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FGit&usg=AOvVaw0mR6q4LmK3M4ft3iKkMUI3)

# A Brief Story of Git

Git was created by Linus Torvalds and fellow open source community members who were working on Linux. Linux kernel was using  BitKeeper as it's VCS since 2002. Bitkeeper was a proprietary distributed version control software (DVCS) which Linux had acquired a non-pay license. The license was given under some restrictions which didn't allow the developers to make a BitKeeper competitor. In 2005, Andrew Tridgell reversed engineered some of the BitKeeper protocols which violated the license terms. BitMover as a reaction revoked the license. An open-sourced project couldn't be supported by a paid service which led the Linux contributors to start developing their won tool for version control.

Although a lot of other alternatives were available at that time, they all lacked a supreme requirement of Linus Torvalds, SPEED. Within a couple of months, git as a complete CVS with all the basic features demanded by a VCS and was lightning fast. Git kept some features of BK like distributed nature and independent development.

BitKeeper went Open Source 11 years later and Git is now the most used VCS.

[https://www.openhub.net/repositories/compare](https://www.openhub.net/repositories/compare)

# Distributed vs Centralized VCS

In a centralized VCS, the repository is hosted on a remote server that stores all the branches and the capability to take the versioning actions on them. All the clients that need tht codebase of a branch have to pull it from the server every time and also commit their changes to the server as well. Hence the availability of the server and a single point of control is atleast requied.

In the distributed VCS, the repository is completely cloned onto the system and is locally accessible and manageable by every client. The branching and committing are done locally and those changes can be synced with the remote repository (if any) via push and pull commands.

![Git%20-%20Beginner/Untitled.png](Git%20-%20Beginner/Untitled.png)

[List of Main Advantages of Distributed Version Control Systems - scriptcrunch](https://scriptcrunch.com/295/)

# Git Local Area

On the local machine, git maintains 3 areas to separate the committed, uncommitted, and intermediate changes.

Local Repo: This area has all the changes and branch history saved in form of objects which is a complete repository in itself. It can be migrated from one system to another or can be synced from the remote system.

Working Directory: This is the local filesystem which is available for you to edit and work upon. You can save, stage, and commit the changes made here to the local repository.

Staging Area: This area is where you can mark some of your changes as staged. These are the changes which are ready to be committed but aren't committed yet. Note that the staging area changes are committed into the local repository, not the working directory.

![Git%20-%20Beginner/Untitled%201.png](Git%20-%20Beginner/Untitled%201.png)

Let's now discuss further the workflow of moving the changes from one area to another.

# Git Workflow

## Git LifeCycle

Before a file in git is added to the repository, it can be in one of the four states:

Untracked: Any file which isn't added to git yet is untracked. A new file when created is always untracked.

Unmodified: Any file which is tracked and is unchanged since it was last staged/committed is called to be an unmodified file.

Modified: Any file which is tracked and is changed since it was last staged/committed is called to be a modified file.

Staged: A modified file when staged to be committed to the repository is knows as a staged file.

A file can be partially in multiple of these states. For example, it is possible for a file to be staged and then modified again. The new changes will then shown be in modified status and old changes will be shown as staged. The files can be moved from one stage to another as depicted in the diagram.

![Git%20-%20Beginner/Untitled%202.png](Git%20-%20Beginner/Untitled%202.png)

This flow is only available for a repository with a working directory. Rest of the data is stored in git branches which we will explore next.

# Branch Structure and HEAD

Git snapshots are stored in a tree structure to support nonlinear development, rolling back, and establishing data integrity. Every commit is a node in the tree and a node can be pointed by a branch, So, a branch is just a pointer to a node in the git tree. This enables the devs to work on different parts of the code parallelly.

![Git%20-%20Beginner/Untitled%203.png](Git%20-%20Beginner/Untitled%203.png)

In this illustration we can see 5 commits in white, 2 branches in orange, the master being the main branch. And, the HEAD is pointing to the master branch which signifies that we are currently working on the master branch.

## Commits and Merge

Now let's discuss what is happening when we are committing a code in git. As illustrated earlier whenever a new branch is added, it creates a new node in the git tree and moves the HEAD of the branch to the new node. Every commit creates some files in the *.git/objects* folder, which are:

- A file to save the directory structure with reference to the blob of each file.
- The blobs of each file
- A commit message with following meta data
    - Message
    - Author
    - Time
    - List of Parent commits (Empty in case of first commit)

Now that we know that these nodes are and what the commits are, we can discuss how multiple devs can merge the work they have done. There can be a few cases while merging, let's look at them one by one.

### Fast Forward

In this case the merging branch is ahead of the current branch and the current branch has no changes that are not in the merging branch. in this case the HEAD of the current branch is simply moved to the new commit. 

![https://res.cloudinary.com/practicaldev/image/fetch/s--cT4TSe48--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/894znjv4oo9agqiz4dql.gif](https://res.cloudinary.com/practicaldev/image/fetch/s--cT4TSe48--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/894znjv4oo9agqiz4dql.gif)

### No Fast Forward

In this case the current branch as well as the merging branch, both have some unmerged changes. In order to merge the merging branch, a new commit is added on the top of the current branch which lists both the commits as it's parents and the HEAD of the current branch is moved to the new commit.

![https://res.cloudinary.com/practicaldev/image/fetch/s--zRZ0x2Vc--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/rf1o2b6eduboqwkigg3w.gif](https://res.cloudinary.com/practicaldev/image/fetch/s--zRZ0x2Vc--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/rf1o2b6eduboqwkigg3w.gif)

### Merge Conflicts

With the **No Fast Forward** case, it might happen that the changes are made to the same files in both of the branches which is conflicting. In that case the user is prompted to fix those issues and make the new merging commit manually, which was made automatically in the previous case.

![https://res.cloudinary.com/practicaldev/image/fetch/s--7lBksXwA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/bcd5ajtoc0g5dxzmpfbq.gif](https://res.cloudinary.com/practicaldev/image/fetch/s--7lBksXwA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/bcd5ajtoc0g5dxzmpfbq.gif)

Source:

[🌳🚀 CS Visualized: Useful Git Commands](https://dev.to/lydiahallie/cs-visualized-useful-git-commands-37p1)

# Remotes, Pull, Push, Fetch, Clone

The distributed nature of this git makes a copy of the repository in the local system which is complete in itself. However still, the developers can set up a single centralized server to host the main repository which then can be cloned to and synced with any other system. Git provides a command each for these actions.

To create a local copy of the repository on the local system. It sets up the cloned repository as the remote repo.

To sync the changes from the remote repo to the local repo, git provides a fetch operation that updates the contents of the remote repo by downloading it. You can also use the pull operation that first performs a fetch operation followed by a merge.

To sync the changes from the local repo to the remote repo, you can use the push operation which will upload the changes you made to the local copy to the remote servers.

![Git%20-%20Beginner/Untitled%204.png](Git%20-%20Beginner/Untitled%204.png)

# Content Addressability, Data Integrity, and Merkle Tree

What's common between technologies like Tor, Blockchain, and Git? All of them are decentralized.

One must have thought at this point that if git is decentralized and the tree can be updated by more than one people which requires the data to be uploaded and downloaded frequently. So how does git identify if a file has changed and how does git maintain the integrity of the data if the data gets corrupted accidentally or intentionally. Git, and many other distributed services, answers this problem using the Merkle tree. The git uses the merkel tree to track changes in a file or a directory. The tree created in git instead of referring to a file location, refers to a blob which is named by the hash of the content, This helps in two ways: the blobs for files with same content are reused and change in hash of a file can determine if a file has changed. This same concept of hashing is used in torrents too. Another way merkel tree helps git to maintain the integrity of the tree is by chaining the commits such that a change in the past commit would invalidate the whole tree below it thus preventing data corruption. The same concept is used in blockchain as well.