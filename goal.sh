#!/bin/bash

# Creating directory for new repository
mkdir git-beginner
cd git-beginner/

# Initializing git
git init

# Creating files and directory structure
mkdir Src
mkdir Src/Pages
mkdir Src/Classes
echo "SampleFile" > Src/Classes/SampleFile.cls
echo "SamplePage" > Src/Pages/SamplePage

# Viewing changes and staging them for commits
git status
git add Src

# Verifying staged files and making initial commit
git status
git commit -m "Initial Commit"

# Viewing commit and merge history
git log

# Updating Sample page
echo "SamplePageChange" > Src/Pages/SamplePage
git status

# Stashing the changes
git stash
git status

# Viewing stashed changes and reapplying them
git stash list
git stash pop

# Adding new file
echo "SamplePage2" > Src/Pages/SamplePage2
git status

# Cleaning the untracked files
git clean -nx
git clean -fx
git status

# Comming the SamplePage changes
git add Src/Pages/SamplePage 
git commit -m "SamplePage Updated"

# Checking out to new branch and then back to master
git checkout -b new-branch
git checkout master

# Updating Sample page (again)
echo "SamplePageChangeAgain" > Src/Pages/SamplePage
git commit -am "SamplePage changed again"
git log

# Updating Sample page (which are not required)
echo "SamplePage Changed not required" > Src/Pages/SamplePage
git status

# Ressetting the non required changes
git reset --hard
git status

# Moving back to previous commit (keeping the changes)
git reset 3231356a056c73dc5af409687b5277263fec8396
git status

# Moving back to previous commit (not keeping the changes)
git reset --hard 3231356a056c73dc5af409687b5277263fec8396
git status

# No entry of the "SamplePage changed again" commit
git log

# Updating Sample page (again after resetting)
echo "SamplePageChangeAgain" > Src/Pages/SamplePage
git status
git commit -am "SamplePage changed again after reset"

# Reverting to previous commit (keeping the changes)
git revert 3231356a056c73dc5af409687b5277263fec8396
git log
cat Src/Pages/SamplePage 
echo "SamplePage Conflict Resolve" > Src/Pages/SamplePage
git status
git add Src/Pages/SamplePage 
git commit -m "Reverted back to previous commit"

# Will have an entry of "SamplePage changed again after reset" unlike reset
git log

# Adding remote url, pushing the local changes and then cloning it into another directory
git remote add origin https://github.com/meta-abhay-verma/git-beginner.git
git push --set-upstream origin master
cd ..
mkdir cloned-repos
cd cloned-repos/
git clone https://github.com/meta-abhay-verma/git-beginner.git
cd git-beginner/
ls